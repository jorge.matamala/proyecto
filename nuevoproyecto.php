<?php
include("conexion.php");

require 'vista.php';


function generarCodigo($longitud) {
 $key = '';
 $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
 $max = strlen($pattern)-1;
 for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
 return $key;
}
 

?>

<!DOCTYPE HTML>
<html lang="en">

<head>
  <title>Nuevo Proyecto</title>
  <link rel="stylesheet" href="estilosss.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>
  

  <div class="encabezado">  
          <h1>NUEVO PROYECTO</h1>
  </div>






    
<form action="agregarproyecto.php"   class="was-validated" enctype="multipart/form-data" method="POST"  type="hidden"  >
  <div class="contenedor3">
    <div class="form-row">
    
      <div class="col-md-4">
        <label for="cod">codigo del proyecto:</label>
       <input type="text" value="<?php echo generarCodigo(6) ?>"  name="nuevcod">
       
      </div>
        

      <div class="col-md-4">
        <label for="nombre">Nombre del proyecto:</label>
        <input type="text" class="form-control" id="proyecto" placeholder="Ingrese nombre del proyecto" name="proyecto" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
   
      <div class="col-md-4">
        <label for="fecha">fecha de inicio:</label>
        <input type="date" class="form-control" id="fe_inicio" placeholder="Ingrese fecha " name="fe_inicio" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
    </div>

    <div class="form-row">
      <div class="col-md-4">
        <label for="fecha_T">fecha de termino:</label>
        <input type="date" class="form-control" id="fe_ter" placeholder="Ingrese fecha" name="fe_ter" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
      <div class="col-md-6">
        <label for="descripcion">descripcion:</label>
        <input type="text" class="form-control" id="descripcion" placeholder="Ingrese una descripcion" name="descripcion" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
    </div>

    <div class="form-row">
     <!-- <div class="col-md-6 mb-3">
          <label for="cuadrilla">documentacion:</label>
          <input type="file" class="form-control" id="documentacion" placeholder="ingrese documentacion" name="archivo" required>
              
          <div class="valid-feedback">Correcto</div>
          <div class="invalid-feedback">Rellene este campo</div>
      </div> -->
      <div class="col-md-6 mb-3">
      <label>tipos de proyectos</label>
             <select name="OS" >  
        <option selected value="0"> Elige una opción </option>
        <?php
        $sql="SELECT * from tipodeproyecto ";
        $result=mysqli_query($con,$sql);
        while($mostrar=mysqli_fetch_array($result)){
        ?>
        
        <option value="1"><?php echo $mostrar['nombre']?></option> 

        <?php
      }
       ?>    
        </select>
      </div>
    </div>

   <button type="submit" class="btn btn-success" style=" margin:0 auto;" >Ingresar</button>
    <input type="button" class="btn btn-danger" value="Cancelar" onclick="history.back() "/>
</div>

</form>


  
</body>
</html>