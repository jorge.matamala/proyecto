<?php
include("conexion.php");
require 'vista.php';
$rut_trabajador=$_GET['user'];

$consulta = mysqli_query ($con, "SELECT * FROM trabajador where rut_trabajador='$rut_trabajador'");
$modificar=mysqli_fetch_array($consulta)

?>

<!DOCTYPE HTML>
<html lang="en">

<head>
	<title>Modificar</title>
  <link rel="stylesheet" href="css/estilosss.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>
	
  <div class="encabezado">  
          <h1>Datos del trabajador</h1>
  </div>

<div class="contenedor3">

<dir class="scroll_vertical">

<form action="modificar.php" method="POST">
    <div class="form-row">
      <div class="col-md-3">
        <label for="rut_trabajador">Rut del trabajador:</label>
        <input type="text" class="form-control" id="rut_trabajador" value="<?php echo $modificar['rut_trabajador']?>" name="rut_trabajador" readonly>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>

      <div class="col-md-4">
        <label for="nombre">Nombre del trabajador:</label>
        <input type="text" class="form-control" id="trabajador" value="<?php echo $modificar['nombre']?>" name="trabajador" >
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
   
      <div class="col-md-4">
        <label for="apellido">Apellidos del trabajador:</label>
        <input type="text" class="form-control" id="apellido" value="<?php echo $modificar['apellido']?>" name="apellido" >
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
    </div>

    <div class="form-row">
      <div class="col-md-3">
        <label for="fecha_naci">Fecha de nacimiento:</label>
        <input type="date" class="form-control" id="fecha_naci" value="<?php echo $modificar['fecha_nacimiento']?>" name="fecha_naci" >
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>

      <div class="col-md-4">
        <label for="correo">Correo electronico:</label>
        <input type="email" class="form-control" id="correo" value="<?php echo $modificar['correo']?>" name="correo">
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
      <div class="col-md-4">
        <label for="telefono">Telefono de contacto:</label>
        <input type="number" class="form-control" id="telefono" value="<?php echo $modificar['telefono']?>" name="telefono" >
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
    </div>

    <div class="form-row">
      <div class="col-md-6">
        <label for="direccion">Direccion de residencia:</label>
        <input type="text" class="form-control" id="direccion" value="<?php echo $modificar['direccion']?>" name="direccion" >
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
      <div class="col-md-4">
          <label for="cuadrilla">Cuadrilla perteneciente:</label>
          <input type="text" class="form-control" id="cod_cuadrilla" value="<?php echo $modificar['cod_cuadrilla']?>" name="cod_cuadrilla" >
          <div class="valid-feedback">Correcto</div>
          <div class="invalid-feedback">Rellene este campo</div>
      </div>
    </div>

<!-- falta formulario de documentacion-->





   <button type="submit" class="btn btn-success" style=" margin:0 auto;" >Ingresar</button>
    <input type="button" class="btn btn-danger" value="Cancelar" onclick="history.back() "/>



</div>
</div>
</div>
</div>
</form>

</dir>

</body>
</html>