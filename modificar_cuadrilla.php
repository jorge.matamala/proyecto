<?php
include("conexion.php");
require 'vista.php';

$cod_cuadrilla=$_GET['user'];

$consulta = mysqli_query ($con, "SELECT * FROM cuadrilla where cod_cuadrilla='$cod_cuadrilla'");
$modificar=mysqli_fetch_array($consulta)


?>

<!DOCTYPE HTML>
<html lang="en">

<head>
  <title>Nueva cuadrilla</title>
  <link rel="stylesheet" href="estilosss.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  
</head>
<body>
  <div class="encabezado">  
    <h1>Datos para cuadrilla</h1>
  </div>

<div class="contenedor3">
  <form action="modificar_cuadrilla2.php" method="POST">
    <div class="form-row">
      <div class="col-md-4">
        <label for="cod_cuadrilla">Codigo de cuadrilla:</label>
        <input type="text" class="form-control" id="cod_cuadrilla" value="<?php echo $modificar['cod_cuadrilla']?>" name="cod_cuadrilla" readonly>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>

      <div class="col-md-3">
        <label for="cantidad">Cantidad de trabajadores:</label>
        <input type="number" class="form-control" id="cantidad" min="1" value="<?php echo $modificar['cantidad']?>" name="cantidad" required >
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
    </div>

<div style="margin-top: 15px;">
    <button type="submit" class="btn btn-success" style=" margin:0 auto;" >Ingresar</button>
    <input type="button" class="btn btn-danger" value="Cancelar" onclick="history.back() "/>
</div>
  </form>
  </div>




</body>
</html>