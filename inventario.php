<?php 
include("conexion.php");
require 'vista.php';
    
   

?>


<!DOCTYPE html>
<html lang="en">
<head>




   
    <title>Lista de materiales</title>
</head>
<body style="background: #CFCBCA;" >

    <div class="encabezado">  
          <h1>Lista de Materiales</h1>
  </div>


<dir style="margin-left: 21%">
      <button type="button" class="btn btn-success" onclick="window.location='agregar_material.php'">agregar material</button>
</dir>



    <div class="contenedor3">
        <div class="table-responsive" style="padding: 1%">
            <table class="table table-striped table-hover" id="inventario">
             
                <thead class="thead-green">
                    <tr>
                       <th scope="col"> Codigo </th> 
                       <th scope="col"> Nombre del material </th> 
                       <th scope="col"> Cantidad </th> 
                       <th scope="col"> Editar</th>
                       
                    </tr>
                </thead>
                <tbody class="tbody-green">
                    <?php
                         $consulta9 = mysqli_query ($con, "SELECT * FROM inventario");
            while($inventario=mysqli_fetch_array($consulta9)){
            
                     ?>
                    <tr>
                        <td> <?php echo $inventario['codigo']  ?> </td>      
                        <td> <?php echo $inventario['nombre']  ?> </td>   
                        <td> <?php echo $inventario['cantidad']  ?> </td> 
                        <td>
                            <div class="row">
                                <div class="col-md-6">
                                <a href="modificar_material.php?codigo=<?php echo $inventario['codigo'] ?>"  class="btn btn-outline-info"> Modificar </a>
                                </div>
                                <div class="col-md-6">
                                <a href="eliminar_material.php?codigo=<?php echo $inventario['codigo'] ?>" class="btn btn-outline-danger" onclick="return eliminar()"> Eliminar </a>

                                </div>
                            </div> 
                        </td>       
                    </tr>

                      <?php }?>
                </tbody>
            </table>
        </div>
    
   
    </div>
    



        <?php require 'extensiones/scripts.php'?>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#inventario').DataTable({
                    language: {
                        search: "Buscar:",
                        paginate: {
                            first: "Primer",
                            previous: "Anterior",
                            next: "Siguiente",
                            last: "Último"
                        },
                        info: "Mostrando del _START_ al _END_ de _TOTAL_ resultados disponibles",
                        emptyTable: "No existen elementos para mostrar en la tabla",
                        infoEmpty: "Mostrando del 0 al 0 de 0 resultados",
                        infoFiltered: "(Filtrado de _MAX_ resultados)",
                        lengthMenu: "Mostrando _MENU_ resultados",
                        loadingRecords: "Cargando...",
                        processing: "Procesando...",
                        zeroRecords: "No se encontraron resultados",
                        aria: {
                            sortAscending: ": Ordenado de forma ascendente",
                            sortDescending: ": Ordenado de forma descendente"
                        }

                    }
                });
            });
        </script>
        <script type="text/javascript"> 
            function eliminar(){
                var respuesta = confirm("¿Estas seguro de eliminar el material?");
                if(respuesta==true){
                    return true;
                 }else{
                    return false;
                }
            } 
        </script>
     
</body>
</html>