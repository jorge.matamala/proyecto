<?php

   include("conexion.php");
    $codigo = $_GET['codigo'];
     require 'vista.php';
    $sql = "SELECT * FROM inventario WHERE codigo=$codigo";
    $resultado = $con->query($sql);
    $resultado = $resultado->fetch_assoc();


?>


<!DOCTYPE html>
<html lang="en">
<head>
    
    <title>Modificar Materiales</title>
</head>
<body style="background: #CFCBCA;">

    <div class="encabezado">
        <h2>Modificar material</h2>
    </div>
    
    <div class="contenedor3">
        <form action="modificar_material2.php" method="POST">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <span><b>Codigo</b></span>
                        <input type="number" class="form-control" id="idcodigo" name='idcodigo' value="<?php echo $resultado['codigo']?>" readonly>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <span><b>Nombre</b></span>
                        <input type="text" class="form-control" id="nombre" name='nombre' value="<?php echo $resultado['nombre']?>" required>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <span><b> Cantidad</b></span>
                        <input type="number" class="form-control" id="cantidad" name='cantidad' min="0" value="<?php echo $resultado['cantidad']?>" required>
                    </div>
                </div>
                
                <div class="col-md-12" >
                    <br>
                    <button type="submit" style='width:20%;' class="btn btn-success" onclick="return modificar()">Aceptar</button>
                    <a href="inventario.php?cancelado" style='width:20%;' id="cancelar" name="cancelar" class="btn btn-danger">Cancelar</a>
                </div>
            </div>

        </form>




    </div>

    <script type="text/javascript"> 
            function modificar(){
                var respuesta = confirm("¿Estas seguro que quieres modificar el material?");
                if(respuesta==true){
                    return true;
                 }else{
                    return false;
                }
            } 
    </script>




 
</body>
</html>
