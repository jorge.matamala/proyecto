<?php

require 'vista.php';

include("conexion.php");


?>



<!DOCTYPE HTML>
<html lang="en">

<head>
	<title>Proyectos</title>
 
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/
  fontello.css">
  <link rel="stylesheet" type="text/css" href="estilosss.css">


</head>
  
  <script type="text/javascript">
    function eliminar(){
      var respuesta = confirm("¿Estas seguro que deseas eliminar proyecto?")
      if ( respuesta== true ){
        return true;
      }
      else {
        return false;
      }
    }


  </script>
<body>
  <div class="encabezado">  
          <h1>Lista de Proyectos </h1>
  </div>


<dir style="margin-left: 21%">
      <button type="button" class="btn btn-success" onclick="window.location='nuevoproyecto.php'">nuevo proyecto</button>
</dir>

<div class="contenedor3">  

  <div class="table-responsive">          
    <table class="table table-striped table-hover" id="tabla">
      <thead class="thead-green">
        <tr>
        <th>Codigo</th>
        <th>Nombre </th>
        <th>Fecha de inicio</th>
        <th>Fecha de termino</th>
        <th>Descripcion</th>
        <th>Tipo de proyecto </th>
        <th>Documentacion</th>
        <th>Editar</th>
        
        </tr>
    </thead>
    <tbody class="tbody-green">
        <?php
      
          $consulta5 = mysqli_query ($con, "SELECT * FROM proyecto");
            while($mostrar5=mysqli_fetch_array($consulta5)){
        ?>
        <tr>
           <td><?php echo $mostrar5['cod_proyecto']?></td>
          <td><?php echo $mostrar5['nombre']?></td>
          <td><?php echo $mostrar5['fecha_inicio']?></td>
          <td><?php echo $mostrar5['fecha_termino']?></td>
           <td><?php echo $mostrar5['descripcion']?></td>
           <td><?php echo $mostrar5['cod_tipo']?></td>
           <td><?php echo $mostrar5['documentacion']?></td>
          <td>
            <div class="row" style="margin-left: 2%">
              <div class="col-md-4">
               
              <div class="col-md-4">
                <a href="modificar_proyecto.php?cod=<?php echo $mostrar5['cod_proyecto']?>" class="btn btn-outline-info">Modificar</a>
              </div>
              <div class="col-md-4">
                <a href="eliminar_proyecto.php?cod=<?php echo $mostrar5['cod_proyecto']?>" class="btn btn-outline-danger" onclick="return eliminar()" >Eliminar</a>
              </div>
            </div>
          </td>
        </tr>
        <?php }?>
    </tbody>
    </table>

  </div>


  </div>

</div>



<?php require 'extensiones/scripts.php'?>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#tabla').DataTable({
                language: {
                    search: "Buscar:",
                    paginate: {
                        first: "Primer",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Último"
                    },
                    info: "Mostrando del _START_ al _END_ de _TOTAL_ resultados disponibles",
                    emptyTable: "No existen elementos para mostrar en la tabla",
                    infoEmpty: "Mostrando del 0 al 0 de 0 resultados",
                    infoFiltered: "(Filtrado de _MAX_ resultados)",
                    lengthMenu: "Mostrando _MENU_ resultados",
                    loadingRecords: "Cargando...",
                    processing: "Procesando...",
                    zeroRecords: "No se encontraron resultados",
                    aria: {
                        sortAscending: ": Ordenado de forma ascendente",
                        sortDescending: ": Ordenado de forma descendente"
                    }

                }
            });
        });
    </script>


</body>
</html>