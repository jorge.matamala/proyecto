<?php
include("conexion.php");

require 'vista.php';


function generarCodigo($longitud) {
 $key = '';
 $pattern = '1234';
 $max = strlen($pattern)-1;
 for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
 return $key;
}
 

?>

<!DOCTYPE HTML>
<html lang="en">

<head>
  <title>Nuevo Tipo de  Proyecto</title>
  <link rel="stylesheet" href="estilosss.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>
  

  <div class="encabezado">  
          <h1>NUEVO TIPO DE  PROYECTO</h1>
  </div>






    
<form    class="was-validated" enctype="multipart/form-data" method="POST"  type="hidden"  >
  <div class="contenedor3">
    <div class="form-row">
    
      <div class="col-md-4">
        <label for="cod">codigo del proyecto:</label>
       <input type="text" value="<?php echo generarCodigo(4) ?>"  name="cod_tipo">
       
      </div>
        

      <div class="col-md-4">
        <label for="nombre">Nombre del tipo proyecto:</label>
        <input type="text" class="form-control" id="proyecto" placeholder="Ingrese nombre del tipo de  proyecto" name="nombre" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
   
      <div class="col-md-6">
        <label for="descripcion">descripcion:</label>
        <input type="text" class="form-control" id="descripcion" placeholder="Ingrese una descripcion" name="descripcion" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
    </div>

    

   
   <button type="submit" class="btn btn-success" style=" margin:0 auto;" >Ingresar</button>
    <input type="button" class="btn btn-danger" value="Cancelar" onclick="history.back() "/>
</div>

</form>
 <script>
        $(document).ready(function(){
            $('form').submit(function(event){
                var formData = {
                    'nombre': $('input[name=nombre]').val()
                   'cod_tipo': $('input[name=cod_tipo]').val()
                   'descripcion': $('input[name=descripcion]').val()
                    
                };

                $.ajax({
                    type: 'POST',
                    url: 'ingresartipo.php',
                    data: formData,
                    dataType: 'json',
                    encode: true
                })
                .done(function(data){
                    if(!data.success){
                        if(data.errors.nombre){
                            Swal.fire({
                                icon: 'error',
                                title: 'Ha ocurrido un error',
                                text: data.errors.nombre
                            })
                        }
                    }else{
                        Swal.fire({
                            icon: 'success',
                            title: 'Registro exitoso',
                            text: "La marca se ha registrado con éxito",

                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Continuar'
                        }).then((result) => {
                            if (result.value) {
                                location.href = "../index.php";
                            }
                        })
                    }
                })
                .fail(function(data){
                    console.log(data);
                });
                event.preventDefault();
            });
        });
    </script>

  
</body>
</html>