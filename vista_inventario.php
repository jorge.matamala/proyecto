<?php 
include 'include/conexion.php';
    $query = "SELECT * FROM inventario";
    $consulta_inventario = $conexion->query($query);


?>


<!DOCTYPE html>
<html lang="en">
<head>




    
    <title>Lista de materiales</title>
</head>
<body style="background: #CFCBCA;" >

    <div class="cuadro">
        <h1>Lista de Materiales</h1>
    </div>

    <div class="contenedor">
        <div class="table-responsive" style="padding: 1%">
            <table class="table table-bordered" id="inventario">
                 <thead>
                    <tr>
                       <th scope="col"> Codigo </th> 
                       <th scope="col"> Nombre del material </th> 
                       <th scope="col"> Cantidad </th>
                       
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($consulta_inventario->num_rows>0){
                            while($inventario = $consulta_inventario->fetch_assoc()){
            
                     ?>
                    <tr>
                        <td> <?php echo $inventario['codigo']  ?> </td>      
                        <td> <?php echo $inventario['nombre']  ?> </td>   
                        <td> <?php echo $inventario['cantidad']  ?> </td>     
                    </tr>

                    <?php
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    
   
    
    



        <?php require 'extensiones/scripts.php'?>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#inventario').DataTable({
                    language: {
                        search: "Buscar:",
                        paginate: {
                            first: "Primer",
                            previous: "Anterior",
                            next: "Siguiente",
                            last: "Último"
                        },
                        info: "Mostrando del _START_ al _END_ de _TOTAL_ resultados disponibles",
                        emptyTable: "No existen elementos para mostrar en la tabla",
                        infoEmpty: "Mostrando del 0 al 0 de 0 resultados",
                        infoFiltered: "(Filtrado de _MAX_ resultados)",
                        lengthMenu: "Mostrando _MENU_ resultados",
                        loadingRecords: "Cargando...",
                        processing: "Procesando...",
                        zeroRecords: "No se encontraron resultados",
                        aria: {
                            sortAscending: ": Ordenado de forma ascendente",
                            sortDescending: ": Ordenado de forma descendente"
                        }

                    }
                });
            });
        </script>
     </div>

    
</body>
</html>