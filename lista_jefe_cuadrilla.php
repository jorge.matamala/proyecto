<?php
include("conexion.php");
require 'vista.php';
?>
<!DOCTYPE HTML>
<html lang="en">

<head>
<script type="text/javascript">
  function eliminar(){
    var respuesta = confirm("¿Esta seguro que desea eliminar al jefe de cuadrilla?");
    if (respuesta==true) {
      return true;
      }else{
      return false;
    }
  }
</script>
	<title>Jefes de cuadrilla</title>
  <link rel="stylesheet" href="estilosss.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

</head>

<body>

  <div class="encabezado">  
          <h1>Lista de Jefes de Cuadrillas</h1>
  </div>

<dir style="margin-left: 21%">
      <button type="button" class="btn btn-success" onclick="window.location='nuevo_jefe_cuadrilla.php'">Nuevo Jefe Cuadrilla</button>
</dir>
      


<div class="contenedor3">
  <div class="table-responsive">          
    <table class="table table-striped table-hover" id="jcua">
      <thead class="thead-green">
        <tr>
        <th>Rut</th>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Telefono</th>
        <th>Correo</th>
        <th>Cuadrilla</th>
        <th>Editar</th>
        </tr>
    </thead>
    <tbody class="tbody-green">
        <?php
          $consulta = mysqli_query ($con, "SELECT * FROM jefe_cuadrilla");
            while($mostrar=mysqli_fetch_array($consulta)){
        ?>
        <tr>
          <td><?php echo $mostrar['rut_jefe_cuadrilla']?></td>
          <td><?php echo $mostrar['nombre']?></td>
          <td><?php echo $mostrar['apellido']?></td>
          <td><?php echo $mostrar['telefono']?></td>
          <td><?php echo $mostrar['correo']?></td>
          <td><?php echo $mostrar['cod_cuadrilla']?> </td>
          <td>
            <div class="row" style="margin-left: 2%">
              <div class="col-md-6">
                <a href="modificar_jefe_cuadrilla.php?user=<?php echo $mostrar['rut_jefe_cuadrilla']?>" class="btn btn-outline-info">Modificar</a>
              </div>
              <div class="col-md-6">
                <a href="eliminar_jefe_cuadrilla.php?user=<?php echo $mostrar['rut_jefe_cuadrilla']?>" class="btn btn-outline-danger"  onclick="return eliminar()">Eliminar</a>
              </div>
            </div>
          </td>
        </tr>
        <?php }?>
    </tbody>
    </table>

</div>
  </div>



<?php require 'extensiones/scripts.php'?>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#jcua').DataTable({
                language: {
                    search: "Buscar:",
                    paginate: {
                        first: "Primer",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Último"
                    },
                    info: "Mostrando del _START_ al _END_ de _TOTAL_ resultados disponibles",
                    emptyTable: "No existen elementos para mostrar en la tabla",
                    infoEmpty: "Mostrando del 0 al 0 de 0 resultados",
                    infoFiltered: "(Filtrado de _MAX_ resultados)",
                    lengthMenu: "Mostrando _MENU_ resultados",
                    loadingRecords: "Cargando...",
                    processing: "Procesando...",
                    zeroRecords: "No se encontraron resultados",
                    aria: {
                        sortAscending: ": Ordenado de forma ascendente",
                        sortDescending: ": Ordenado de forma descendente"
                    }

                }
            });
        });
    </script>





</body>
</html>