<?php
include("conexion.php");
require 'vista.php';
$rut_trabajador=$_GET['user'];

$consulta = mysqli_query ($con, "SELECT * FROM trabajador where rut_trabajador='$rut_trabajador'");
$visualizar=mysqli_fetch_array($consulta)

?>

<!DOCTYPE HTML>
<html lang="en">

<head>
	<title>Trabajador</title>
  <link rel="stylesheet" href="estilosss.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>
	

	
<div class="contenedor3">
  <div class="table-responsive">          
    <table class="table table-striped table-hover">
    <thead class="thead-green">
        <tr>
        <th>Rut</th>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Fecha de nacimiento</th>
        <th>Correo electronico</th>
        <th>Telefono</th>
        <th>Direccion</th>
        <th>cod_cuadrilla</th>
        </tr>
    </thead>
    <tbody class="tbody-green">    
        <tr>
          <td><?php echo $visualizar['rut_trabajador']?></td>
          <td><?php echo $visualizar['nombre']?></td>
          <td><?php echo $visualizar['apellido']?></td>
          <td><?php echo $visualizar['fecha_nacimiento']?></td>
          <td><?php echo $visualizar['correo']?></td> 
          <td><?php echo $visualizar['telefono']?></td>
          <td><?php echo $visualizar['direccion']?></td> 
          <td><?php echo $visualizar['cod_cuadrilla']?></td>
        </tr>
    </tbody>
    </table>
    <table class="table table-striped table-hover">
      <thead class="thead-green">
        <tr>
        <th>Contrato</th>
        <th>curriculum</th>
        <th>certificado_antecedentes</th>
        <th>afp</th>
        <th>cert_salud</th>
        </tr>
    </thead>
    <tbody class="tbody-green">
        <tr>
          <td><?php echo $visualizar['contrato']?></td>
          <td><?php echo $visualizar['curriculum']?></td>
          <td><?php echo $visualizar['certificado_antecedentes']?></td>
          <td><?php echo $visualizar['afp']?></td>
          <td><?php echo $visualizar['cert_salud']?></td>
        </tr>
    </tbody>
    </table>

<dir style="margin-left: 40%">
      <button type="button" class="btn btn-primary" onclick="window.location='lista_trabajadores.php'">Volver a la lista</button>
</dir>

  </div>
</div>


</body>
</html>
