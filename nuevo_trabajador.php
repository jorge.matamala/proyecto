<?php
include("conexion.php");
require 'vista.php';
?>

<!DOCTYPE HTML>
<html lang="en">

<head>
  <title>Nuevo trabajador</title>
  <link rel="stylesheet" href="estilosss.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  
</head>
<body>


  <div class="encabezado">  
    <h1>Datos del trabajador</h1>
  </div>

<div class="contenedor3">

  <form action="agregar.php" method="POST" enctype="multipart/form-data">
    <div class="form-row">
      <div class="col-md-3">
        <label for="rut_trabajador">Rut del trabajador:</label>
        <input type="text" class="form-control" required oninput="checkRut(this)" id="rut_trabajador" placeholder="Ingrese rut" name="rut_trabajador" required >
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>

      <div class="col-md-4">
        <label for="nombre">Nombre trabajador:</label>
        <input type="text" class="form-control" id="trabajador" placeholder="Ingrese nombre" name="trabajador" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>

      <div class="col-md-4">
        <label for="apellido">Apellidos del trabajador:</label>
        <input type="text" class="form-control" id="apellido" placeholder="Ingrese apellidos" name="apellido" required >
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
    </div>

    <div class="form-row">
      <div class="col-md-3">
        <label for="fecha_naci">Fecha de nacimiento:</label>
        <input type="date" class="form-control" id="fecha_naci" placeholder="Ingrese fecha" name="fecha_naci" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>

      <div class="col-md-4">
        <label for="correo">Correo electronico:</label>
        <input type="email" class="form-control" id="correo" placeholder="example@correo.com" name="correo" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
      <div class="col-md-4">
        <label for="telefono">Telefono de contacto:</label>
        <input type="number" class="form-control" id="telefono" placeholder="Ingrese telefono" name="telefono" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
    </div>

    <div class="form-row">
      <div class="col-md-6">
        <label for="direccion">Direccion de residencia:</label>
        <input type="text" class="form-control" id="direccion" placeholder="Ingrese direccion" name="direccion"required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
      <div class="col-md-4">
        <label for="cuadrilla">Cuadrilla perteneciente:</label>
        <input type="text" class="form-control" id="cod_cuadrilla" placeholder="codigo" name="cod_cuadrilla"required >
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
    </div>





<!--  linea de formulario documento-->

    <div class="form-row">
      <div class="col-md-3">
        <label for="contrato">Contrato de trabajo:</label>
        <input type="file" class="form-control" id="contrato" name="contrato" accept="application/pdf" required>
      </div>

      <div class="col-md-3">
        <label for="curriculum">Curriculum del trabajador:</label>
        <input type="file" class="form-control" id="curriculum" name="curriculum" accept="application/pdf"required>     
      </div>

      <div class="col-md-4">
        <label for="antecedentes">Certificado de antecedentes:</label>
        <input type="file" class="form-control" id="antecedentes" name="antecedentes" accept="application/pdf"required>
      </div>

      <div class="col-md-3">
        <label for="afp">Certificado de afiliacion:</label>
        <input type="file" class="form-control" id="afp" name="afp" accept="application/pdf"required>
      </div>

      <div class="col-md-3">
        <label for="salud">certificado de salud:</label>
        <input type="file" class="form-control" id="salud" name="salud" accept="application/pdf"required>
      </div>
   
    </div>
        <div style="margin-top: 32px; ">
    <button type="submit" class="btn btn-success" style=" margin:0 auto;" >Ingresar</button>
    <input type="button" class="btn btn-danger" value="Cancelar" onclick="history.back() "/>
  </div>
  </div>
</div>
</div>
</div>
</form>

</dir>




  <script type="text/javascript">
function checkRut(rut) {
    // Despejar Puntos
    var valor = rut.value.replace('.','');
    // Despejar Guión
    valor = valor.replace('-','');
    
    // Aislar Cuerpo y Dígito Verificador
    cuerpo = valor.slice(0,-1);
    dv = valor.slice(-1).toUpperCase();
    
    // Formatear RUN
    rut.value = cuerpo + '-'+ dv
    
    // Si no cumple con el mínimo ej. (n.nnn.nnn)
    if(cuerpo.length < 7 || cuerpo.length > 8) { rut.setCustomValidity("Rut no valido"); return false;}
    
    // Calcular Dígito Verificador
    suma = 0;
    multiplo = 2;
    
    // Para cada dígito del Cuerpo
    for(i=1;i<=cuerpo.length;i++) {
    
        // Obtener su Producto con el Múltiplo Correspondiente
        index = multiplo * valor.charAt(cuerpo.length - i);
        
        // Sumar al Contador General
        suma = suma + index;
        
        // Consolidar Múltiplo dentro del rango [2,7]
        if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
  
    }
    
    // Calcular Dígito Verificador en base al Módulo 11
    dvEsperado = 11 - (suma % 11);
    
    // Casos Especiales (0 y K)
    dv = (dv == 'K')?10:dv;
    dv = (dv == 0)?11:dv;
    
    // Validar que el Cuerpo coincide con su Dígito Verificador
    if(dvEsperado != dv) { rut.setCustomValidity("RUT no existe"); return false; }
    
    // Si todo sale bien, eliminar errores (decretar que es válido)
    rut.setCustomValidity('');
}

</script>

</body>
</html>
